-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 07:28 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matrix_mall`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `user_id`, `alamat`) VALUES
('C001', 2, 'Ds. Cebolek Kidul RT 02 RW 06, Kec. Margoyoso, Kab. Pati'),
('C002', 7, 'Ds. Cebolek Kidul RT 02 RW 06'),
('C003', 11, 'Ds. Cebolek Kidul RT 02 RW 06'),
('C004', 14, 'Margoyoso'),
('C005', 16, 'Samudra pasifik'),
('C006', 17, 'Condong Catur');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE `jenis_produk` (
  `id` int(11) NOT NULL,
  `jenis_toko` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_produk`
--

INSERT INTO `jenis_produk` (`id`, `jenis_toko`, `nama`) VALUES
(1, 1, 'Makanan'),
(2, 1, 'Minuman');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_toko`
--

CREATE TABLE `jenis_toko` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_toko`
--

INSERT INTO `jenis_toko` (`id`, `nama`) VALUES
(1, 'Restoran');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` varchar(255) NOT NULL,
  `supplier_id` varchar(255) NOT NULL,
  `jenis_produk` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `supplier_id`, `jenis_produk`, `nama`, `harga`, `status`) VALUES
('P001', 'S001', 1, 'Pindang Serani', 8000, 0),
('P002', 'S001', 2, 'Es Teh', 2000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_toko` int(11) NOT NULL,
  `slogan_toko` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `user_id`, `alamat`, `jenis_toko`, `slogan_toko`) VALUES
('S001', 13, 'Margoyoso', 1, 'Pindang serani wenak tenan'),
('S002', 15, 'Selatan kali kecing', 1, 'Sego goreng mambu vanilla'),
('S003', 19, 'Gunung sing duwur', 1, 'Keduwuren cuy');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `supplier_id` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `tanggal_trx` datetime NOT NULL,
  `produk_id` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` enum('admin','supplier','customer') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `email`, `password`, `role`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'Muhammad Faizud Darion', 'ensignbandeng', 'fa@faizud.net', '2fecffa8a8a35c6c8bc6973915e54c3e', 'customer'),
(7, 'Muhammad Nabil', 'mnabilfa', 'mnabilfa@faizud.net', '2e7bd80e510a7f040a6791ef3bce5b80', 'customer'),
(11, 'Akhmad Ghozi Syarofi Ariq', 'agsaagsa', 'agsa@agsa.net', '6cc53f2bfa78725bb308bba0d0c124dc', 'customer'),
(13, 'Bakul Pindang', 'bakulpindang', 'bakul@pindang.com', '2fecffa8a8a35c6c8bc6973915e54c3e', 'supplier'),
(14, 'Bakharudin Yusuf', 'deleteme', 'deleteme@gmail.com', 'f06d22b607bc47dad31df5c2bb89d0e3', 'customer'),
(15, 'Sego Goreng Mantap', 'segodigoreng', 'segodi@goreng.com', '23a46dfda9f0619b3430c4b23b665f76', 'supplier'),
(16, 'Iwak Bandeng', 'C005', 'iwak@bandeng.org', '2fecffa8a8a35c6c8bc6973915e54c3e', 'customer'),
(17, 'Muhammad Fahrur', 'C006', 'fai@masfai.com', '1655463bb3c36f3e6e678d2747d065c6', 'customer'),
(19, 'Pindang Serani Istimewa', 'S003', 'pindang@pindangserani.com', '2df8ff23a4e27cd4949e1230240f81e8', 'supplier');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_toko`
--
ALTER TABLE `jenis_toko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jenis_toko`
--
ALTER TABLE `jenis_toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
