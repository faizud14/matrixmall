<?php if(Session::has('login')) echo "<meta http-equiv='refresh' content='1;url=/'" ?>

@extends('layouts.master')

@section('title', 'Login')

@section('content')
    <h2>Login to your account</h2>
    <div class="login-container">
        <form style="align-content: center;" action="/login" method="post">
            
            @if(isset($username))
                <p style="background-color:#4CAF50; padding: 5px; color: #fff">Username mu adalah {{ $username }}</p>
            @endif

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="username">Username</label>
            <input class="control" type="text" name="username" id="username" placeholder="username">
    
            <label for="password">Password</label>
            <input class="control" type="password" name="password" id="password" placeholder="Password">

            <br/>
            <input class="control" type="submit" value="Login" name="Login">
        </form>
    </div>
@endsection