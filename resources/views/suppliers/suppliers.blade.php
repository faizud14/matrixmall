<?php
$supplierData = DB::table('supplier')
->join('user', 'supplier.user_id', 'user.id')
->join('jenis_toko', 'supplier.jenis_toko', 'jenis_toko.id')
->select(DB::raw('supplier.id as id, user.nama as nama, jenis_toko.nama as jenis, supplier.alamat as alamat, supplier.slogan_toko as slogan'))
->get();
?>
@extends('layouts.master')

@section('title', 'Find Suppliers')

@section('content')
    <h2>Suppliers list</h2>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis Toko</th>
                <th>Alamat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($supplierData as $key => $value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->jenis}}</td>
                    <td>{{$value->alamat}}</td>
                    <td><a href="/suppliers/{{$value->id}}/products">View products</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection