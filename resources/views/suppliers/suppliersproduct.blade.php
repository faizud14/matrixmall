<?php 
$supplierData = DB::table('supplier')->where('id', $id)->first();
$productData = DB::table('produk')
->join('supplier', 'produk.supplier_id','=', 'supplier.id')
->join('user', 'user.id', '=', 'supplier.user_id')
->join('jenis_produk', 'produk.jenis_produk', '=', 'jenis_produk.id')
->join('jenis_toko', 'jenis_produk.jenis_toko', '=', 'jenis_toko.id')
->select(DB::raw('produk.id as pid, user.nama as namasup, supplier.id as supid, jenis_produk.nama as jenis, 
produk.nama as nama, produk.harga as harga, produk.status as status'))
->where('supplier.id', '=', $supplierData->id)
->get();
$userData = DB::table('user')->where('id', $supplierData->user_id)->first();
$customerData = DB::table('user')->where('id', Session::get('login'))->first();
?>
@extends('layouts.master')

@section('title', 'Product | Matrix Mall')

@section('content')
    <table class="table">
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>Jenis</th>
            <th>Harga</th>
            <th>Action</th>
        </thead>
        <tbody>
            <p>Hi Mr. {{$customerData->nama}}, welcome to {{$userData->nama}}</p>
            <p>{{$supplierData->slogan_toko}}</p>
            <br>
            <p>Address: {{$supplierData->alamat}}</p>
            @foreach($productData as $key=>$value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->jenis }}</td>
                    <td>{{ $value->harga }}</td>
                    <td>
                        <a href="/suppliers/{{$value->supid}}/buy/{{ $value->pid }}">Buy</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection