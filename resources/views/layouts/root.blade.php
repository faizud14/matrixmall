<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    {{ HTML::style('css/style.css') }}
</head>
<body>
    <nav class="navbar">
        <ul>
            <li><a href="/">Home</a></li>
            @yield('account')
        </ul>
    </nav>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>