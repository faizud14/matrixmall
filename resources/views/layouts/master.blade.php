<?php
if(Session::has('login')) {
    $user = DB::select("select * from user where id=?", [Session::get('login')]);
}
?>
@extends('layouts.root')

@section('account')
    @if(Session::has('login'))
        @switch($user[0]->role)
            @case("admin")
                <li><a href="/suppliers">Suppliers</a></li>
                @break
            @case("customer")
                <li><a href="/suppliers">Suppliers</a></li>
                @break
            @case("supplier")
                <li><a href="/product">Products</a></li>
                <li><a href="/transaction">Transaction</a></li>
                @break
            @default
                @break
        @endswitch
        <li style="float: right"><a href="/profile/{{ $user[0]->role }}">Profile</a></li>
        <li style="float: right"><a href="/logout">Logout</a></li>
        <p style="background-color: rgb(113, 187, 255); float:right; color: #fff; padding: 10px; margin: 0;">Logged in as {{ $user[0]->nama }}</p>
    @else
        <li style="float: right"><a href="/login">Login</a></li>
        <li style="float: right"><a href="#">Register</a>
            <ul>
                <li><a href="/register/customer">Customer</a></li>
                <li><a href="/register/supplier">Supplier</a></li>
            </ul>
        </li>
    @endif
@endsection