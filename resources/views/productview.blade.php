<?php 
$productData = DB::table('produk')->where('id', $id)->get();
$jenisData = DB::table('jenis_produk')->where('id', $productData[0]->jenis_produk)->first();
?>
@extends('layouts.master')

@section('title', 'Product View')

@section('content')

    <h2>Editing {{ $productData[0]->nama }}...</h2>

    <form action="/product/edit/{{ $id }}" method="post" class="form-container">

        @if (count($errors) > 0)
            <div class = "alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p style="background-color:brown; padding: 5px; color: #fff">{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <label for="nama">Nama</label>
        <input value="{{$productData[0]->nama}}" type="text" name="nama" id="nama" class="control">

        <label for="jenis">Jenis</label>
        <select name="jenis" id="jenis" class="control">
            <option value="{{ $productData[0]->jenis_produk }}">{{$jenisData->nama}}</option>
        </select>

        <label for="harga">Harga</label>
        <input value="{{$productData[0]->harga}}" type="text" name="harga" id="harga" class="control">

        <label for="status">Status</label>
        <select name="status" id="status" class="control">
            <option {{($productData[0]->status==0)?"checked":""}} value="0">Not Available</option>
            <option {{($productData[0]->status==1)?"checked":""}} value="1">Available</option>
        </select>

        <input type="submit" value="Save" class="control">
    </form>
@endsection