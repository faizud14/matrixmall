<?php
use Illuminate\Support\Facades\DB;
if(isset($user)) {
    $namauser=$user[0]->nama;
    $alamat = "";
    $email =$user[0]->email;
    switch($user[0]->role) {
        case "supplier":
            $supplier = DB::table('supplier')->where('user_id', $user[0]->id)->get();
            $alamat = $supplier[0]->alamat;
            break;
        case "customer":
            $customer = DB::table('customer')->where('user_id', $user[0]->id)->get();
            $alamat = $customer[0]->alamat;
            break;
        case "admin":
            break;
        default: break;
    }
    $jenis = DB::table('jenis_toko')->get();
}
?>
@extends('layouts.master')

@section('title', $user[0]->nama.' | Profile')

@section('content')
    @if(isset($user))
        <div class="form-container">
            <form style="align-content: center;" action="/profile/{{ $user[0]->role }}" method="post">
    
                @if(isset($errormessages))
                    <p style="background-color:brown; padding: 5px; color: #fff">{{ $errormessages }}</p>
                @endif
    
                @if (count($errors) > 0)
                    <div class = "alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p style="background-color:brown; padding: 5px; color: #fff">{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
    
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="userid" value="{{ $user[0]->id }}">
    
                <label for="nama">Nama</label>
                <input value="{{ $user[0]->nama }}" class="control" type="text" name="nama" id="nama" placeholder="Nama">
    
                @if($user[0]->role!="admin")
                    <label for="alamat">Alamat</label>
                    <input value="{{ $alamat }}" class="control" type="text" name="alamat" id="alamat" placeholder="Alamat">
                @endif
                
                @if($user[0]->role=="supplier")
                    <label for="slogan">Slogan</label>
                    <input value="{{$supplier[0]->slogan_toko}}" class="control" type="text" name="slogan" id="slogan" placeholder="Slogan">

                    <label for="jenis">Jenis toko</label>
                    <select class="control" name="jenis" id="jenis">
                        @foreach($jenis as $key=>$values)
                            <option {{ $values->id==$supplier[0]->jenis_toko?"selected":""}} value="{{ $values->id }}">{{ $values->nama }}</option>
                        @endforeach
                    </select>   
                @endif

                <label for="email">Email</label>
                <input value="{{ $user[0]->email }}" class="control" type="text" name="email" id="email" placeholder="Email">
                
                <label for="oldpassword">Old Password</label>
                <input class="control" type="password" name="oldpassword" id="oldpassword" placeholder="Old Password">

                <label for="password">Password</label>
                <input class="control" type="password" name="password" id="password" placeholder="Password">
    
                <label for="confirmpass">Confirm Password</label>
                <input class="control" type="password" name="confirmpass" id="confirmpass" placeholder="Confirm Password">
    
                <br/>
                <input class="control" type="submit" value="Save" name="Save">
            </form>
        </div>
    @endif
@endsection