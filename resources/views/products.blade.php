<?php 
if(Session::has('login')) {
    $supplierData = DB::table('supplier')->where('user_id', Session::get('login'))->first();
    $productData = DB::table('produk')
    ->join('supplier', 'produk.supplier_id','=', 'supplier.id')
    ->join('user', 'user.id', '=', 'supplier.user_id')
    ->join('jenis_produk', 'produk.jenis_produk', '=', 'jenis_produk.id')
    ->join('jenis_toko', 'jenis_produk.jenis_toko', '=', 'jenis_toko.id')
    ->select(DB::raw('produk.id as pid, user.nama as namasup, supplier.id as supid, jenis_produk.nama as jenis, 
    produk.nama as nama, produk.harga as harga, produk.status as status'))
    ->where('supplier.id', '=', $supplierData->id)
    ->get();
    $userData = DB::table('user')->where('id', $supplierData->user_id)->first();
}
?>
@extends('layouts.master')

@section('title', 'Product | Matrix Mall')

@section('content')
    <table class="table">
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>Jenis</th>
            <th>Harga</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
            <h2>Your products... {{$userData->nama}}</h2>
            @foreach($productData as $key=>$values)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $values->nama }}</td>
                    <td>{{ $values->jenis }}</td>
                    <td>{{ $values->harga }}</td>
                    <td>{{ ($values->status==0)?"Not Available":"Available" }}</td>
                    <td>
                        <a href="product/edit/{{ $values->pid }}">Edit</a> | 
                        <a href="product/change-status/{{ $values->pid }}">Change Status</a> | 
                        <a href="product/delete/{{ $values->pid }}" onclick="return confirm('are you sure?')">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr><td colspan="6"><a style="text-align: center" href="/product/add" class="control">Add</a></td></tr>
        </tfoot>
    </table>
@endsection