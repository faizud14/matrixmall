<?php
use Illuminate\Support\Facades\DB;
$jenis = DB::table('jenis_toko')->get();
$lastid = DB::table('supplier')->orderBy('id', 'desc')->first();
if($lastid==null)
{
    $lastid = "S001";
} else {
    $lastid = sprintf("S%'.03d", substr($lastid->id, 1)+1);
}
?>
@extends('layouts.master')

@section('title', 'Register Supplier')

@section('content')
    <div class="form-container">
        <form style="align-content: center;" action="/register/supplier" method="post">

            <p style="background-color:#4CAF50; padding: 5px; color: #fff">Username mu adalah {{ $lastid }}</p>

            @if(isset($errormessages))
                <p style="background-color:brown; padding: 5px; color: #fff">{{ $errormessages }}</p>
            @endif

            @if (count($errors) > 0)
                <div class = "alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p style="background-color:brown; padding: 5px; color: #fff">{{ $error }}</p>
                    @endforeach
                </div>
            @endif

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="nama">Nama</label>
            <input class="control" type="text" name="nama" id="nama" placeholder="Nama">

            <label for="alamat">Alamat</label>
            <input class="control" type="text" name="alamat" id="alamat" placeholder="Alamat">

            <label for="slogan">Slogan</label>
            <input class="control" type="text" name="slogan" id="slogan" placeholder="Slogan">

            <label for="jenis">Jenis toko</label>
            <select class="control" name="jenis" id="jenis">
                @foreach($jenis as $key=>$values)
                    <option value="{{ $values->id }}">{{ $values->nama }}</option>
                @endforeach
            </select>

            <label for="email">Email</label>
            <input class="control" type="text" name="email" id="email" placeholder="Email">
    
            <label for="password">Password</label>
            <input class="control" type="password" name="password" id="password" placeholder="Password">

            <label for="confirmpass">Confirm Password</label>
            <input class="control" type="password" name="confirmpass" id="confirmpass" placeholder="Confirm Password">

            <br/>
            <input class="control" type="submit" value="Register" name="Register">
        </form>
    </div>
@endsection