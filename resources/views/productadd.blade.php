<?php 
$jenisData = DB::table('jenis_produk')->get();
?>
@extends('layouts.master')

<?php
$supplierData = DB::table('supplier')->where('user_id', Session::get('login'))->first();
$lastid = DB::table('produk')->where('supplier_id', $supplierData->id)->orderBy('id', 'desc')->first();
if($lastid==null)
{
    $lastid = "no id";
}
else {
    $lastid=$lastid->id;
}
?>

@section('title', 'Product Add')

@section('content')

    <h2>Add new product</h2>

    <form action="/product/add" method="post" class="form-container">

        <p style="background-color:#4CAF50; padding: 5px; color: #fff"> Last product Id: {{ $lastid }}</p>

        @if (count($errors) > 0)
            <div class = "alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p style="background-color:brown; padding: 5px; color: #fff">{{ $error }}</p>
                @endforeach
            </div>
        @endif

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <label for="pid">Product Id</label>
        <input type="text" name="pid" id="pid" class="control">

        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama" class="control">

        <label for="jenis">Jenis</label>
        <select name="jenis" id="jenis" class="control">
            @foreach($jenisData as $key=>$values)
                <option value="{{ $values->id }}">{{$values->nama}}</option>
            @endforeach
        </select>

        <label for="harga">Harga</label>
        <input type="text" name="harga" id="harga" class="control">

        <label for="status">Status</label>
        <select name="status" id="status" class="control">
            <option value="0">Not Available</option>
            <option value="1">Available</option>
        </select>

        <input type="submit" value="Save" class="control">
    </form>
@endsection