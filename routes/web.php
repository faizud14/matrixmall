<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Session::has('login')) {
        $user = DB::select("select * from user where id=?", [Session::get('login')]);
        $nama = $user;
        return view('home', array('nama'=>$nama));
    } else {
        return view('home');
    }
});
Route::get('/home', function () {
    return redirect('/');
});
Route::get('/login', function() {
    if(Session::has('login')) {
        return redirect('home');
    }
    else {
        return view('login');
    }
});
Route::get('/register/customer', function() {
    return view('registercus');
});
Route::post('/register/customer','CustomerController@insert');
Route::get('/register/supplier', function() {
    return view('registersup');
});
Route::post('/register/supplier','SupplierController@insert');
Route::get('/logout', 'UserController@logout');
Route::post('/login', 'UserController@login');
Route::get('/profile/customer', function() {
    if(Session::has('login')) {
        $user = DB::select("select * from user where id=?", [Session::get('login')]);
        return view('profile', array('user'=>$user));
    }
    else {
        return view('home');
    }
});
Route::post('/profile/customer', 'CustomerController@update');
Route::get('/profile/supplier', function() {
    if(Session::has('login')) {
        $user = DB::select("select * from user where id=?", [Session::get('login')]);
        return view('profile', array('user'=>$user));
    }
    else {
        return view('home');
    }
});
Route::post('/profile/supplier', 'SupplierController@update');
Route::get('/product', function() {
    return view('products');
});
Route::get('/product/edit/{id}', 'ProductController@index');
Route::post('/product/edit/{id}', 'ProductController@update');
Route::get('/product/add', function() {
    return view('productadd');
});
Route::post('/product/add', 'ProductController@insert');
Route::get('/product/change-status/{id}', 'ProductController@changeStatus');
Route::get('/product/delete/{id}', 'ProductController@delete');
Route::get('/suppliers', function(){return view('suppliers.suppliers');});
Route::get('/suppliers/{id}/products', 'SuppliersProductController@index');