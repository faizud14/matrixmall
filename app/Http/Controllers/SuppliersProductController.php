<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SuppliersProductController extends Controller
{
    public function index($id){
        return view('suppliers.suppliersproduct', array('id'=>$id));
    }
}
