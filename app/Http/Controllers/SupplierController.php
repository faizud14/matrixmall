<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    //INSERT
    public function insert(Request $request) {
        $this->validate($request, [
            'nama'      =>'required',
            'alamat'    =>'required',
            'email'     =>'required|email|max:255',
            'password'  =>'required|min:6',
            'slogan'    =>'required',
            'jenis'     =>'required',
            'confirmpass'=>'required'
        ]);
        try {
            $lastid = DB::table('supplier')->orderBy('id', 'desc')->first();
            if($lastid==null)
            {
                $lastid = "S001";
            } else {
                $lastid = sprintf("S%'.03d", substr($lastid->id, 1)+1);
            }
            if($request->password!=$request->confirmpass) {
                return view('/register/supplier', array('errormessages'=>"Confirm Password doesn't match"));
            }
            DB::table('user')->insert(
                [
                    'nama'      => $request->nama,
                    'username'  => $lastid,
                    'email'     => $request->email,
                    'password'  => md5($request->password),
                    'role'      => 'supplier'
                ]
            );
            $userid = DB::table('user')->max('id');
            DB::table('supplier')->insert(
                [
                    'id'        => $lastid,
                    'user_id'   => $userid,
                    'alamat'    => $request->alamat,
                    'jenis_toko'=> $request->jenis,
                    'slogan_toko'=>$request->slogan
                ]
            );
            return view('/login', array('username'=>$lastid));
        }
        catch (\Exception $e) {
            echo "Error: ".$e->getMessage();
            echo "<br><br><a href='/register/supplier'>Try Again</a>";
        }
    }

    //UPDATE
    public function update(Request $request) {
        if(!empty($request->password)) {
            $this->validate($request, [
                'userid'    =>'required',
                'nama'          =>'required',
                'alamat'        =>'required',
                'email'         =>'required|email|max:255',
                'oldpassword'   =>'required|min:6',
                'password'      =>'required|min:6',
                'slogan'    =>'required',
                'jenis'     =>'required',
                'confirmpass'   =>'required'
            ]);
            $user = DB::table('user')->where('id', $request->userid)->get();
            if(md5($request->oldpassword)!=$user[0]->password) {
                return view('profile', array('errormessages'=>"Wrong password", 'user'=>$user));
            }
            if($request->password!=$request->confirmpass) {
                return view('profile', array('errormessages'=>"Confirm Password doesn't match", 'user'=>$user));
            }
        }
        else {
            $this->validate($request, [
                'userid'        =>'required',
                'nama'          =>'required',
                'alamat'        =>'required',
                'email'         =>'required|email|max:255',
                'oldpassword'   =>'required|min:6',
                'slogan'    =>'required',
                'jenis'     =>'required',
            ]);
            $user = DB::table('user')->where('id', $request->userid)->get();
            if(md5($request->oldpassword)!=$user[0]->password) {
                return view('profile', array('errormessages'=>"Wrong password", 'user'=>$user));
            }
        }
        try {
            $user = DB::table('user')->where('id', $request->userid)->get();
            DB::table('user')
                ->where('id', $request->userid)
                ->update([
                    'nama'      => $request->nama,
                    'username'  => $user[0]->username,
                    'email'     => $request->email,
                    'password'  => !empty($request->password)?md5($request->password):md5($request->oldpassword),
                    'role'      => 'supplier'
                ]);
            DB::table('supplier')
                ->where('user_id', $request->userid)
                ->update([
                    'alamat' => $request->alamat,
                    'jenis_toko'=> $request->jenis,
                    'slogan_toko'=>$request->slogan
                ]);
            echo "Successfully updated";
            return redirect('/');
        }
        catch (\Exception $e) {
            echo "Error: ".$e->getMessage(); 
        }
    }
}
