<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index($id) {
        return view('productview', array('id'=>$id));
    }

    public function insert(Request $request) {
        $this->validate($request, [
            'pid'       => 'required',
            'nama'      => 'required',
            'jenis'     => 'required',
            'harga'     => 'required|numeric',
            'status'    => 'required'
        ]);
        try {
            $supplier = DB::table('supplier')->where('user_id', $request->session()->get('login'))->first();
            DB::table('produk')
            ->insert([
                'id'    => $request->pid,
                'supplier_id'=>$supplier->id,
                'nama'  => $request->nama,
                'jenis_produk' => $request->jenis,
                'harga' => $request->harga,
                'status'=> $request->status
            ]);
            return redirect('/product');
        } catch(\Exception $e) {
            echo "Error: ".$e->getMessage();
        }
    }

    public function changeStatus($id) {
        try {
            $data = DB::table('produk')->where('id', $id)->first();
            DB::table('produk')->where('id', $id)
            ->update([
                'status'=>($data->status==0)?1:0
            ]);
            return redirect('/product');
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'nama'      => 'required',
            'jenis'     => 'required',
            'harga'     => 'required|numeric',
            'status'    => 'required'
        ]);
        try{
            DB::table('produk')
            ->where('id', '=', $id)
            ->update([
                'nama'  => $request->nama,
                'jenis_produk' => $request->jenis,
                'harga' => $request->harga,
                'status' => $request->status
            ]);
            return redirect('/product');
        }
        catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    public function delete($id) {
        try {
            DB::table('produk')->where('id', '=', $id)->delete();
            return redirect('/product');
        }
        catch(\Exception $e) {
            report($e);
            return false;
        }
    }
}
