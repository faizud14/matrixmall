<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function login(Request $request) {
        $username = $request->username;
        $password = $request->password;
        $user = DB::select('select * from user where (username=? or email=?) and password=?', [$username, $username, md5($password)]);
        if(!empty($user)) {
            $request->session()->put('login', $user[0]->id);
            return redirect('home');
        }
        else {
            return view('login');
        }
    }

    public function logout(Request $request) {
        $request->session()->forget('login');
        return redirect('home');
    }
}
